<%@ taglib uri="/jstl/core" prefix="c"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF8">
    <title>Main</title>
</head>
<body>

    <c:if test="${not empty errorMessage}">
        <c:out value="${errorMessage}"/><br>
    </c:if>

<%--Text string.--%>
    <c:if test="${empty book}">
        <c:out value="There is no one Books."/><br>
    </c:if>

<%--Table.--%>
    <TABLE border="1">

<%--Table head.--%>
    <thead>
        <th width="100">Book name</th>
        <th width="100">Book author</th>
        <th width="100">Page number</th>
    </thead>

<%--Table body.--%>
    <tbody>
    <c:forEach var="book" items="${book}">
    <TR>
        <TD>${book.name}</TD>
        <TD>${book.author}</TD>
        <TD>${book.pageNumber}</TD>
    </TR>
    </c:forEach>
    </tbody>

    </TABLE>
</body>
</html>
