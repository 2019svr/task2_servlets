<%@ page import="com.epam.training.constants.ConstantsJSP" %>
<%@ taglib uri="/jstl/core" prefix="c"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF8">
    <title>Start</title>
  </head>
  <body>

  <c:if test="${not empty errorMessage}">
    <c:out value="${errorMessage}"/>
  </c:if>
  <% request.setAttribute("jump_login", ConstantsJSP.JUMP_LOGIN); %>
  <a href="<c:url value="${jump_login}"/>">Login</a>&nbsp;

  <H4>
    Task2_JavaEE<br><br>

    Login to see the books on JSP.<br>
    Application Name, Available servlets in console.<br>

  </H4>

  </body>
</html>
