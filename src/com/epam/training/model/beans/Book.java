package com.epam.training.model.beans;

import java.io.Serializable;

public class Book implements Serializable {
    private static final long serialVersionUID = 1L;

    private int bookID;
    private String bookName;
    private String bookAuthor;
    private int bookPageNumber;

    public Book(int bookID, String name, String author, int pageNumber) {
        this.bookID = bookID;
        this.bookName = name;
        this.bookAuthor = author;
        this.bookPageNumber = pageNumber;
    }

    public int getBookID() {
        return bookID;
    }

    public String getName() {
        return bookName;
    }

    public void setName(String name) {
        this.bookName = name;
    }

    public String getAuthor() {
        return bookAuthor;
    }

    public void setAuthor(String author) {
        this.bookAuthor = author;
    }

    public int getPageNumber() {
        return bookPageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.bookPageNumber = pageNumber;
    }
}
