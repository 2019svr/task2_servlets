package com.epam.training.model.impl;

import com.epam.training.constants.ConstantsError;
import com.epam.training.constants.DAOLevelConstants;
import com.epam.training.exceptions.DaoException;
import com.epam.training.model.beans.User;
import com.epam.training.model.connections.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SQLUserImpl {

    private static final Logger LOGGER = Logger.getLogger(SQLUserImpl.class.getName());

    public SQLUserImpl() {
    }

    /**
     * Method return user (if exist) from DB according to parameters
     * @param login - login
     * @param password - password
     * @return - user (if exist) from DB
     * @throws DaoException - DaoException
     */
    public User getUser(String login, String password) throws DaoException {

        try (Connection cn = ConnectionPool.getConnection();
             PreparedStatement ps = cn.prepareStatement(DAOLevelConstants.SELECT_LOGIN)) {

            ps.setString(DAOLevelConstants.LOGIN_FIELD_INDEX, login);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    String name = rs.getString(DAOLevelConstants.LOGIN_FIELD_INDEX);
                    String pswd = rs.getString(DAOLevelConstants.PSWD_FIELD_INDEX);

                    if (!pswd.equals(password)) {
                        throw new DaoException(ConstantsError.ERROR_WRONG_LOGIN_OR_PASSWORD);
                    }
                    return newUser(name, password);
                } else {
                    throw new DaoException(ConstantsError.ERROR_LOGIN_NOT_EXIST);
                }

            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, ConstantsError.ERROR_CONNECTION_DB, e);
            throw new DaoException(e);
        }
    }

    private User newUser(String name, String password) {
        return new User(name, password);
    }

}
