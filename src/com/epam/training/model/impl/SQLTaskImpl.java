package com.epam.training.model.impl;

import com.epam.training.exceptions.DaoException;
import com.epam.training.model.beans.Book;
import com.epam.training.model.beans.User;
import com.epam.training.model.connections.ConnectionPool;
import com.epam.training.model.helpers.BookFieldsDemonstrate;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQLTaskImpl {
    public SQLTaskImpl() {
    }

    /**
     * Method demonstrate information about books (book name,
     * book author, pages number.
     * @param user - user
     * @return = books list
     * @throws DaoException - DaoException
     */
    public List<Book> getBooks(User user) throws DaoException {
        BookFieldsDemonstrate bfd = new BookFieldsDemonstrate();
        List<Book> books = new ArrayList<>();

        try (
             Connection cn = ConnectionPool.getConnection();
             PreparedStatement ps = bfd.prepareStatement(cn, user.getName());
             ResultSet rs = ps.executeQuery()
        ){

            while (rs.next()) {
                String name = rs.getString(1);
                String author = rs.getString(2);
                int pageNumber = rs.getInt(3);
                int ID = rs.getInt(4);
                books.add(new Book(ID, name, author, pageNumber));
            }
            return books;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }
}
