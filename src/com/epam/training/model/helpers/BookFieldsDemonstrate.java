package com.epam.training.model.helpers;

import com.epam.training.constants.DAOLevelConstants;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BookFieldsDemonstrate extends AbstractStatementPreparer {

    /**
     * PreparedStatement method to take information
     *  about books from DB
     * @param cn - connection
     * @param login - login
     * @return - prepareStatement
     * @throws SQLException - SQLException
     */
    public PreparedStatement prepareStatement(Connection cn, String login) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(DAOLevelConstants.SELECT_BOOKS);
        setParameters(ps, login);
        return ps;
    }
}