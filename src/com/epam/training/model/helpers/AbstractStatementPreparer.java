package com.epam.training.model.helpers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class AbstractStatementPreparer {

    public abstract PreparedStatement prepareStatement(Connection cn, String login) throws SQLException;

    /**
     * Method to make preparedStatement from different number of objects from DB
     * @param ps - preparedStatement
     * @param objects - DB objects
     * @throws SQLException - SQLException
     */
    void setParameters(PreparedStatement ps, Object... objects) throws SQLException {
        for (int i = 0; i < objects.length; i++) {
            ps.setObject(i + 1, objects[i]);
        }
    }
}
