package com.epam.training.constants;

public final class Constants {

    public static final String SLASH = "/";
    public static final String MAIN_PAGE = "main.jsp";
    public static final String LOGIN_PAGE = "login.jsp";
    public static final String MAIN_CONTROLLER = "main";
    public static final String KEY_ERROR_MESSAGE = "errorMessage";

}