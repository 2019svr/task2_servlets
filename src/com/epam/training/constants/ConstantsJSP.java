package com.epam.training.constants;

public class ConstantsJSP {

    public static final String KEY_LOGIN = "login";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_USER = "user";
    public static final String KEY_TASKS = "book";
    public static final String JUMP_LOGIN = "/login.jsp";
    public static final String JUMP_START = "/start.jsp";

}
