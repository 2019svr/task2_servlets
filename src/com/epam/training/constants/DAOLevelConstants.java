package com.epam.training.constants;

public class DAOLevelConstants {

    public static final int LOGIN_FIELD_INDEX = 1;
    public static final int PSWD_FIELD_INDEX = 2;
    public static final String SELECT_LOGIN = "SELECT name, pswd FROM logins WHERE name = ?";
    public static final String SELECT_BOOKS = "SELECT  book.name, book.author, book.pageNumber, "
            + "book.idBook FROM book INNER JOIN logins ON book.idLogin = logins.idLogin WHERE (logins.name = ?)";
}
