package com.epam.training.controllers.Listeners;

import javax.servlet.*;
import javax.servlet.annotation.WebListener;
import java.util.logging.Logger;

@WebListener
public class ApplicationInfoListener implements ServletContextListener {

    private static final Logger LOG = Logger.getLogger(ApplicationInfoListener.class.getName());

    /**
     * Method output information to console
     * @param servletContextEvent - servletContextEvent
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        LOG.info("Server info: " + servletContextEvent
                .getServletContext().getServerInfo());
        LOG.info("App name: " + servletContextEvent
                .getServletContext().getServletContextName());
        LOG.info("Available Servlets: " + String.join("; ", servletContextEvent
                .getServletContext().getServletRegistrations().keySet()));
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        LOG.info("Server is down");
    }
}
