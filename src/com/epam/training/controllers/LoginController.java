package com.epam.training.controllers;

import com.epam.training.constants.Constants;
import com.epam.training.constants.ConstantsJSP;
import com.epam.training.exceptions.DaoException;
import com.epam.training.model.beans.User;
import com.epam.training.model.impl.SQLUserImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.ValidationException;
import java.io.IOException;

public class LoginController extends BaseController {

    private static final long serialVersionUID = 5L;

    /**
     * Post method that redirects page to controllers/MainController.java
     * @param request - request
     * @param response - response
     * @throws ServletException - a servlet encounters difficulty
     * @throws IOException - an input or output operation is failed or interpreted
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String login = request.getParameter(ConstantsJSP.KEY_LOGIN);
        String password = request.getParameter(ConstantsJSP.KEY_PASSWORD);
        SQLUserImpl sqlUser = new SQLUserImpl();
        try {
            CheckLoginPass.CorrectLoginPass(login, password);
            User user = sqlUser.getUser(login, password);
            request.getSession().setAttribute(ConstantsJSP.KEY_USER, user);
            redirectPage(Constants.MAIN_CONTROLLER, response);

        }catch (DaoException | ValidationException e) {
            forwardError(Constants.SLASH + Constants.LOGIN_PAGE, e.getMessage(), request, response);
            return;
        }

    }
}
