package com.epam.training.controllers;

import com.epam.training.constants.Constants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class BaseController extends HttpServlet {

    private static final long serialVersionUID = 6L;

     /**
     * Method redirects confirm to url
     * @param url - url
     * @param response - response
     * @throws ServletException - a servlet encounters difficulty
     * @throws IOException - an input or output operation is failed or interpreted
     */
    protected void redirectPage(String url, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect(url);
    }

     /**
     * Method forwards confirm to url
     * @param url - url
     * @param request - request
     * @param response - response
     * @throws ServletException - a servlet encounters difficulty
     * @throws IOException - an input or output operation is failed or interpreted
     */
    protected void forward(String url, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
        rd.forward(request, response);
    }

     /**
     * Method forwards error message confirm to url
     * @param url - url
     * @param message - message
     * @param request - request
     * @param response - response
     * @throws ServletException - a servlet encounters difficulty
     * @throws IOException - an input or output operation is failed or interpreted
     */
    protected void forwardError(String url, String message, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute(Constants.KEY_ERROR_MESSAGE, message);
        forward(url , request, response);
    }
}