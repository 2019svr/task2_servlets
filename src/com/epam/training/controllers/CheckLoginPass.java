package com.epam.training.controllers;

import com.epam.training.constants.ConstantsError;

import javax.xml.bind.ValidationException;

public class CheckLoginPass {

    /**
     * Method check login & password
     * @param login
     * @param password
     * @throws ValidationException - shows message: - Login or password are empty.
     */
    public static void CorrectLoginPass(String login, String password) throws ValidationException {
        if (login == null || password == null) {
            throw new ValidationException(ConstantsError.ERROR_EMPTY);
        }
        login = login.trim();
        password = password.trim();
        if ("".equals(login) || "".equals(password)) {
            throw new ValidationException(ConstantsError.ERROR_EMPTY);
        }
    }
}
