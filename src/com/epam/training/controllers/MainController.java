package com.epam.training.controllers;

import com.epam.training.constants.Constants;
import com.epam.training.constants.ConstantsJSP;
import com.epam.training.exceptions.DaoException;
import com.epam.training.model.beans.Book;
import com.epam.training.model.beans.User;
import com.epam.training.model.impl.SQLTaskImpl;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class MainController extends BaseController{

    private static final long serialVersionUID = 3L;
    private AtomicBoolean permanent;
    private int seconds;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        permanent = new AtomicBoolean(false);
        seconds = 3;
    }

     /**
     * Get method that forwards to main.jsp
     * @param request - request
     * @param response - response
     * @throws ServletException - a servlet encounters difficulty
     * @throws IOException - an input or output operation is failed or interpreted
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if(!permanent.get()){
            permanent.set(true);
            throw new UnavailableException("Stopped for testing", seconds);
        }

        User user = (User)request.getSession().getAttribute(ConstantsJSP.KEY_USER);
        SQLTaskImpl sqlTask = new SQLTaskImpl();

        try {
            List<Book> book = sqlTask.getBooks(user);
            request.getSession().setAttribute(ConstantsJSP.KEY_TASKS, book);
            redirectPage(Constants.MAIN_PAGE, response);
            return;
        }catch (DaoException e) {
            forwardError(Constants.SLASH + Constants.MAIN_PAGE, e.getMessage(), request, response);
            return;
        }
    }
}